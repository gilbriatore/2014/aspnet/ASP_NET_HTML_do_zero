﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Cadastro()
        {
            Pessoa p = new Pessoa();
            p.Nome = "João";
            p.DataNascimento = new DateTime(2000, 5,20);
            return View(p);
        }

        public ActionResult Sobre()
        {
            return View();
        }

	}
}